;(function(){

  function cfFormAddField( $timeout){
    return {
      restrict: 'A',
      scope : '=',
      link: function( scope, element, attrs){
        element.click(function(){
          var field = {
            default_text: '',
            help_text: '', 
            required: false,
            options: [],
            type: attrs.cfFormAddField
          }

          scope.content.fields.push( field);
          $timeout(function(){
            scope.$apply();
          })
        });
      }
    }
  }

  function cfFormAddOption( $timeout){
    return {
      restrict: 'A',
      scope : '=',
      require: 'ngModel',
      link: function( scope, element, attrs, ngModel){
        element.click(function(){
          var option = {};
          var field = ngModel.$viewValue;
          field.options.push( option);
          ngModel.$setViewValue( field)
          $timeout(function(){
            scope.$apply();
          })
        });
      }
    }
  }

  function cfFormFields( $timeout){
    return {
        restrict: 'A',
        scope: '=',
        require: 'ngModel',
        link: function( scope, element, attrs, ngModel){
          if( !scope.sortableFields){
            scope.sortableFields = {
              handle: '.handle-field',
              items: '.field-container',
              axis: 'y',
              distance: 20            
            }
          }

          scope.$watch( attrs['ngModel'], function( newValue){
            var fields = scope.$eval( attrs ['ngModel']);
            for( var i = 0; i < fields.length; i++){
              fields[i].position = (i + 1);
            }
            
            $timeout(function(){
              scope.$apply();
            });
          }, true);
        }
    };
  }

  function cfFormOptions( $timeout){
    return {
        restrict: 'A',
        scope: '=',
        require: 'ngModel',
        link: function( scope, element, attrs, ngModel){
          if( !scope.sortableOptions){
            scope.sortableOptions = {
              handle: '.handle-option',
              items: '.cf-form-option',
              axis: 'y',
              distance: 20            
            }
          }

          scope.$watch( attrs['ngModel'], function( newValue){
            var fields = scope.$eval( attrs ['ngModel']);
            for( var i = 0; i < fields.length; i++){
              fields[i].position = (i + 1);
            }
            
            $timeout(function(){
              scope.$apply();
            });
          }, true);
        }
    };
  }

  /**
   *
   * Pass all functions into module
   */
  angular
      .module('admin')
      .directive( 'cfFormAddField', cfFormAddField)
      .directive( 'cfFormAddOption', cfFormAddOption)
      .directive( 'cfFormFields', cfFormFields)
      .directive( 'cfFormOptions', cfFormOptions)
  ;
   
})()