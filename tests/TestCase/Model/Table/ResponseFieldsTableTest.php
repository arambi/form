<?php
namespace Form\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Form\Model\Table\ResponseFieldsTable;

/**
 * Form\Model\Table\ResponseFieldsTable Test Case
 */
class ResponseFieldsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.form.response_fields',
        'plugin.form.responses',
        'plugin.form.forms',
        'plugin.form.forms_title_translation',
        'plugin.form.forms_after_finish_text_translation',
        'plugin.form.forms_submit_text_translation',
        'plugin.form.forms_i18n',
        'plugin.form.fields',
        'plugin.form.options',
        'plugin.form.options_title_translation',
        'plugin.form.fields_title_translation',
        'plugin.form.fields_help_text_translation',
        'plugin.form.fields_default_text_translation'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ResponseFields') ? [] : ['className' => 'Form\Model\Table\ResponseFieldsTable'];
        $this->ResponseFields = TableRegistry::get('ResponseFields', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ResponseFields);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
