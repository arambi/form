<?php
namespace Form\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Form\Model\Table\FieldsTable;
use Manager\TestSuite\CrudTestCase;


/**
 * Form\Model\Table\FieldsTable Test Case
 */
class FieldsTableTest extends CrudTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.form.fields'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Fields') ? [] : ['className' => 'Form\Model\Table\FieldsTable'];
        $this->Fields = TableRegistry::get('Fields', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Fields);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->Fields);
        $this->assertCrudDataIndex( 'index', $this->Fields);
      }
}
