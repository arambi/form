<?php
namespace Form\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Form\Model\Table\OptionsTable;
use Manager\TestSuite\CrudTestCase;


/**
 * Form\Model\Table\OptionsTable Test Case
 */
class OptionsTableTest extends CrudTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.form.options'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Options') ? [] : ['className' => 'Form\Model\Table\OptionsTable'];
        $this->Options = TableRegistry::get('Options', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Options);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->Options);
        $this->assertCrudDataIndex( 'index', $this->Options);
      }
}
