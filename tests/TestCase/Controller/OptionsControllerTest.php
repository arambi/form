<?php
namespace Form\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;
use Form\Controller\OptionsController;

/**
 * Form\Controller\OptionsController Test Case
 */
class OptionsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.form.options'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
