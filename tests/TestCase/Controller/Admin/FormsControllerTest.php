<?php
namespace Form\Test\TestCase\Controller\Admin;

use Cake\TestSuite\IntegrationTestCase;
use Form\Controller\Admin\FormsController;

/**
 * Form\Controller\Admin\FormsController Test Case
 */
class FormsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.form.forms'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
