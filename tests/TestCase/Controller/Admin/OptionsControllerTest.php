<?php
namespace Form\Test\TestCase\Controller\Admin;

use Cake\TestSuite\IntegrationTestCase;
use Form\Controller\Admin\OptionsController;

/**
 * Form\Controller\Admin\OptionsController Test Case
 */
class OptionsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.form.options'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
