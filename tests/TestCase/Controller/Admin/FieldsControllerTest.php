<?php
namespace Form\Test\TestCase\Controller\Admin;

use Cake\TestSuite\IntegrationTestCase;
use Form\Controller\Admin\FieldsController;

/**
 * Form\Controller\Admin\FieldsController Test Case
 */
class FieldsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.form.fields'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
