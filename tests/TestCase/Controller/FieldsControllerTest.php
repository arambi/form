<?php
namespace Form\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;
use Form\Controller\FieldsController;

/**
 * Form\Controller\FieldsController Test Case
 */
class FieldsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.form.fields'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
