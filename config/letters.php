<?php 

use Letter\Collector\LetterCollector;


LetterCollector::add( 'Form.new', [
  'title' => 'Nuevo formulario recibido (admin)',
  'subject' => [
    'spa' => 'Nuevo formulario de {form_name} recibido en {web_name}'
  ],
  'file' => 'form_new',
  'vars' => [
    'form_name' => 'El nombre del formulario',
    'web_name' => 'Nombre del web',
    'data' => 'Los datos del formulario'
  ]
]);

LetterCollector::add( 'Form.new_user', [
  'title' => 'Nuevo formulario recibido (usuario)',
  'subject' => [
    'spa' => 'Gracias por enviar el formulario de {form_name} en {web_name}'
  ],
  'file' => 'form_new_client',
  'vars' => [
    'form_name' => 'El nombre del formulario',
    'web_name' => 'Nombre del web',
    'data' => 'Los datos del formulario'
  ]
]);
