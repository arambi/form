<?php 
use Section\Action\ActionCollection;
use Manager\Navigation\NavigationCollection;
use User\Auth\Access;
use Block\Lib\BlocksRegistry;

require 'letters.php';



Access::add( 'forms', [
  'name' => 'Formularios (edición)',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Form',
          'controller' => 'Forms',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Form',
          'controller' => 'Forms',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Form',
          'controller' => 'Forms',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Form',
          'controller' => 'Forms',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Form',
          'controller' => 'Forms',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Form',
          'controller' => 'Fields',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Form',
          'controller' => 'Fields',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Form',
          'controller' => 'Fields',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Form',
          'controller' => 'Fields',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Form',
          'controller' => 'Fields',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Form',
          'controller' => 'Options',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Form',
          'controller' => 'Options',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Form',
          'controller' => 'Options',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Form',
          'controller' => 'Options',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Form',
          'controller' => 'Options',
          'action' => 'delete',
        ],
      ]
    ]
  ]
]);



Access::add( 'responses', [
  'name' => 'Formularios (respuestas)',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Form',
          'controller' => 'Responses',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Form',
          'controller' => 'Responses',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Form',
          'controller' => 'Responses',
          'action' => 'delete',
        ],
      ]
    ]
  ]
]);



NavigationCollection::add( [
  'name' => 'Edición de Formularios',
  'parentName' => 'Formularios',
  'plugin' => 'Form',
  'controller' => 'Forms',
  'action' => 'index',
  'icon' => 'fa fa-file-text-o',
]);

NavigationCollection::add( [
  'name' => 'Respuestas de Formularios',
  'parentName' => 'Formularios',
  'plugin' => 'Form',
  'controller' => 'Responses',
  'action' => 'index',
  'icon' => 'fa fa-reply-all',
]);

// Bloque Formulario
BlocksRegistry::add( 'form', [
    'key' => 'form',
    'title' => __d( 'admin', 'Formulario'),
    'icon' => 'fa fa-file-text-o',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => false,
    'deletable' => true,   
    'className' => 'Form\\Model\\Block\\FormBlock',
    'cell' => 'Form.Form::display',
    'blockView' => 'Form/blocks/form'
]);

