<?php

use Migrations\AbstractMigration;

class FormsLegalText extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('form_forms')
            ->addColumn('legal_text', 'text', ['default' => null, 'null' => true])
            ->update();

        $this->table('form_forms_translations')
            ->addColumn('legal_text', 'text', ['default' => null, 'null' => true])
            ->update();
    }
}
