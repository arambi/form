<?php
use Migrations\AbstractMigration;

class FormsEmailFrom extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('form_forms')
            ->addColumn('custom_email_from_bool', 'boolean', ['default' => 0, 'null' => false])
            ->addColumn('custom_email_from', 'string', ['default' => null, 'null' => true])
            ->update();
    }
}
