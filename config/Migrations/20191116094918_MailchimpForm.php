<?php
use Migrations\AbstractMigration;

class MailchimpForm extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $forms = $this->table( 'form_forms');
    $forms
        ->addColumn( 'mailchimp_list_id', 'string', ['limit' => 64, 'null' => true, 'default' => null])
        ->addColumn( 'has_mailchimp_list', 'boolean', ['null' => false, 'default' => 0])
        ->addColumn( 'field_auth_mailchimp_id', 'integer', ['null' => true, 'default' => null])
        ->update();
  
    $fields = $this->table( 'form_fields');
    $fields
          ->addColumn( 'mailchimp_field_id', 'string', [ 'default' => null, 'null' => true])
          ->addColumn( 'mailchimp_accept', 'boolean', [ 'default' => 0, 'null' => false])
          ->update();
  }
}
