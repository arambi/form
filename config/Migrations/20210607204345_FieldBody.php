<?php
use Migrations\AbstractMigration;

class FieldBody extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('form_fields')
            ->addColumn('body', 'text', ['default' => null, 'null' => true])
            ->update();

        $this->table('form_fields_translations')
            ->addColumn('body', 'text', ['default' => null, 'null' => true])
            ->update();
    }
}
