<?php

use Phinx\Migration\AbstractMigration;

class KeyForm extends AbstractMigration
{

  public function change()
  {
    $forms = $this->table( 'form_forms');
    $forms
          ->addColumn( 'cname', 'string', [ 'limit' => 128, 'default' => null, 'null' => true])
          ->save();
  }
}
