<?php

use Phinx\Migration\AbstractMigration;

class Responses extends AbstractMigration
{
    
  public function up()
  {
    $responses = $this->table( 'form_responses');
    $responses
          ->addColumn( 'form_id', 'integer', [ 'default' => null, 'null' => true])
          ->addColumn( 'ip', 'string', array( 'limit' => 64, 'default' => null))
          ->addColumn( 'salt', 'string', array( 'limit' => 64, 'default' => null))
          ->addColumn( 'created', 'datetime', array( 'default' => null))
          ->save();

    $response_fields = $this->table( 'form_response_fields');
    $response_fields
          ->addColumn( 'response_id', 'integer', [ 'default' => null, 'null' => true])
          ->addColumn( 'field_id', 'text', [ 'default' => null, 'null' => true])
          ->addColumn( 'value', 'text', array( 'limit' => 64, 'default' => null))
          ->addColumn( 'created', 'datetime', array( 'default' => null))
          ->save();
  }

  public function down()
  {
    $this->dropTable( 'form_responses');
    $this->dropTable( 'form_response_fields');
  }
}
