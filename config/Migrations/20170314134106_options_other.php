<?php

use Phinx\Migration\AbstractMigration;

class OptionsOther extends AbstractMigration
{

  public function change()
  {
    $options = $this->table( 'form_options');
    $options
          ->addColumn( 'other', 'boolean', [ 'default' => null, 'null' => true])
          ->addColumn( 'other_title', 'string', [ 'limit' => 255, 'default' => null, 'null' => true])
          ->save();

    $response_fields = $this->table( 'form_response_fields');
    $response_fields
          ->addColumn( 'value_other', 'text', array( 'limit' => 64, 'default' => null))
          ->save();
  }
}
