<?php

use Phinx\Migration\AbstractMigration;

class TitleOption extends AbstractMigration
{
  public function change()
  {
    $forms = $this->table( 'form_forms');
    $forms
          ->addColumn( 'show_title', 'boolean', [ 'default' => null, 'null' => false])
          ->save();
  }
}
