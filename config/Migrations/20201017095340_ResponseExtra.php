<?php

use Migrations\AbstractMigration;

class ResponseExtra extends AbstractMigration
{

    public function change()
    {
        $responses = $this->table('form_responses');
        $responses
            ->addColumn('extra', 'text', ['default' => null, 'null' => true])
            ->update();
    }
}
