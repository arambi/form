<?php

use Phinx\Migration\AbstractMigration;

class Placeholder extends AbstractMigration
{
  public function up()
  {
    $forms = $this->table( 'form_fields');
    $forms
          ->addColumn( 'placeholder', 'boolean', [ 'default' => null, 'null' => true])
          ->update();
  }

  public function down()
  {
    $forms = $this->table( 'form_fields');
    $forms->removeColumn( 'placeholder');
  }
}
