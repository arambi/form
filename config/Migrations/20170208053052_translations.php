<?php

use Phinx\Migration\AbstractMigration;

class Translations extends AbstractMigration
{
  public function up()
  {
    $forms = $this->table( 'form_forms_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $forms
      ->addColumn( 'id', 'integer', ['null' => false])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
      ->addColumn( 'title', 'string', [ 'limit' => 255, 'default' => null])
      ->addColumn( 'after_finish_text', 'text', [ 'default' => null, 'null' => true])
      ->addColumn( 'submit_text', 'string', [ 'limit' => 255, 'default' => null, 'null' => true])
      ->save();  

    $fields = $this->table( 'form_fields_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $fields
      ->addColumn( 'id', 'integer', ['null' => false])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
      ->addColumn( 'title', 'string', [ 'limit' => 255, 'default' => null, 'null' => true])
      ->addColumn( 'default_text', 'string', [ 'limit' => 255, 'default' => null, 'null' => true])
      ->addColumn( 'help_text', 'string', [ 'limit' => 255, 'default' => null, 'null' => true])
      ->save();

    $fields = $this->table( 'form_options_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $fields
      ->addColumn( 'id', 'integer', ['null' => false])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
      ->addColumn( 'title', 'string', [ 'limit' => 255, 'default' => null, 'null' => true])
      ->save();  
  }

  public function down()
  {
    $this->dropTable( 'form_forms_translations');
    $this->dropTable( 'form_fields_translations');
    $this->dropTable( 'form_options_translations');
  }
}
