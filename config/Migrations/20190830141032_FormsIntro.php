<?php
use Migrations\AbstractMigration;

class FormsIntro extends AbstractMigration
{
  public function change()
  {
    $forms = $this->table( 'form_forms');
    $forms
      ->addColumn( 'body', 'text', ['default' => null, 'null' => true])
      ->update();

    $forms_translations = $this->table( 'form_forms_translations');
    $forms_translations
      ->addColumn( 'body', 'text', ['default' => null, 'null' => true])
      ->update();  
  }
}
