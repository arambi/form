<?php

use Migrations\AbstractMigration;

class CustomMessageMail extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('form_forms')
            ->addColumn('custom_mail_body', 'boolean', ['default' => 0, 'null' => false])
            ->addColumn('mail_body', 'text', ['default' => null, 'null' => true])
            ->update();

        $this->table('form_forms_translations')
            ->addColumn('mail_body', 'text', ['default' => null, 'null' => true])
            ->update();
    }
}
