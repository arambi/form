<?php

use Phinx\Migration\AbstractMigration;

class Initial extends AbstractMigration
{
  /**
   * Change Method.
   *
   * Write your reversible migrations using this method.
   *
   * More information on writing migrations is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
   
  public function change()
  {

  }
  */
 
 /**
   * Migrate Up.
   */
  public function up()
  {
    $forms = $this->table( 'form_forms');
    $forms
          ->addColumn( 'title', 'string', array( 'limit' => 255, 'default' => null, 'null' => true))
          ->addColumn( 'after_finish_option', 'string', [ 'limit' => 64, 'default' => null, 'null' => true])
          ->addColumn( 'after_finish_text', 'text', [ 'default' => null, 'null' => true])
          ->addColumn( 'after_finish_section_id', 'integer', ['limit' => 8, 'default' => null, 'null' => true])
          ->addColumn( 'email_response', 'string', [ 'limit' => 255, 'default' => null, 'null' => true])
          ->addColumn( 'submit_text', 'string', [ 'limit' => 255, 'default' => null, 'null' => true])
          ->addColumn( 'salt', 'string', array( 'limit' => 64, 'default' => null))
          ->addColumn( 'created', 'datetime', array( 'default' => null))
          ->addColumn( 'modified', 'datetime', array( 'default' => null))
          ->save();

    $fields = $this->table( 'form_fields');
    $fields
          ->addColumn( 'title', 'string', [ 'limit' => 255, 'default' => null, 'null' => true])
          ->addColumn( 'form_id', 'integer', [ 'limit' => 255, 'default' => null, 'null' => true])
          ->addColumn( 'default_text', 'string', [ 'limit' => 255, 'default' => null, 'null' => true])
          ->addColumn( 'help_text', 'string', [ 'limit' => 255, 'default' => null, 'null' => true])
          ->addColumn( 'required', 'boolean', [ 'default' => null, 'null' => true])
          ->addColumn( 'validation_method', 'string', array( 'limit' => 32, 'default' => null, 'null' => true))
          ->addColumn( 'type', 'string', array( 'limit' => 16, 'default' => null, 'null' => true))
          ->addColumn( 'position', 'integer', ['limit' => 8, 'default' => null, 'null' => true])
          ->addColumn( 'salt', 'string', array( 'limit' => 64, 'default' => null))
          ->addColumn( 'created', 'datetime', array('default' => null))
          ->addColumn( 'modified', 'datetime', array( 'default' => null))
          ->addIndex( ['form_id'])
          ->addIndex( ['position'])
          ->save();

    $options = $this->table( 'form_options');
    $options
          ->addColumn( 'title', 'string', [ 'limit' => 255, 'default' => null, 'null' => true])
          ->addColumn( 'field_id', 'integer', [ 'limit' => 255, 'default' => null, 'null' => true])
          ->addColumn( 'by_default', 'boolean', [ 'default' => null, 'null' => true])
          ->addColumn( 'salt', 'string', array( 'limit' => 64, 'default' => null))
          ->addColumn( 'position', 'integer', ['limit' => 8, 'default' => null, 'null' => true])
          ->addColumn( 'created', 'datetime', array('default' => null))
          ->addColumn( 'modified', 'datetime', array( 'default' => null))
          ->addIndex( ['field_id'])
          ->addIndex( ['position'])
          ->save();

    $i18n = $this->table( 'form_i18n');
    $i18n
          ->addColumn( 'locale', 'string', ['limit' => 5])
          ->addColumn( 'model', 'string', ['limit' => 64, 'null' => false])
          ->addColumn( 'foreign_key', 'string', ['limit' => 36, 'null' => false])
          ->addColumn( 'field', 'string', ['limit' => 64, 'null' => false])
          ->addColumn( 'content', 'text', ['default' => null, 'null' => true])
          ->addColumn( 'created', 'datetime', array('default' => null))
          ->addColumn( 'modified', 'datetime', array('default' => null))
          ->addIndex( ['locale', 'model', 'foreign_key', 'field'], ['unique' => true])
          ->addIndex( ['locale', 'model', 'foreign_key'], ['unique' => false])
          ->addIndex( ['locale', 'model'], ['unique' => false])
          ->addIndex( ['model', 'foreign_key', 'field'], ['unique' => false])
          ->addIndex( ['model', 'foreign_key'], ['unique' => false])
          ->save();
  }

  /**
   * Migrate Down.
   */
  public function down()
  {
    $this->dropTable( 'form_forms');
    $this->dropTable( 'form_fields');
    $this->dropTable( 'form_options');
    $this->dropTable( 'form_i18n');
  }
}
