<?php
use Migrations\AbstractMigration;

class FormUriReferer extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $responses = $this->table( 'form_responses');
    $responses
      ->addColumn( 'uri_referer', 'string', [ 'default' => null, 'null' => true])
      ->update();
  }
}
