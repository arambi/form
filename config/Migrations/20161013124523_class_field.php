<?php

use Phinx\Migration\AbstractMigration;

class ClassField extends AbstractMigration
{

  public function change()
  {
    $forms = $this->table( 'form_fields');
    $forms
          ->addColumn( 'class_name', 'string', [ 'default' => null, 'null' => true])
          ->save();
  }
}
