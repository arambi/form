<?php

use Phinx\Migration\AbstractMigration;

class Privacy extends AbstractMigration
{

  public function up()
  {
    $forms = $this->table( 'form_forms');
    $forms
          ->addColumn( 'privacy_section_id', 'integer', ['limit' => 8, 'default' => null, 'null' => true])
          ->update();
  }

  public function down()
  {
    $forms = $this->table( 'form_forms');
    $forms->removeColumn( 'privacy_section_id');
  }
}
