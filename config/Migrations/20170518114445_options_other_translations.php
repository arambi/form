<?php

use Phinx\Migration\AbstractMigration;

class OptionsOtherTranslations extends AbstractMigration
{
   
  public function change()
  {
    $fields = $this->table( 'form_options_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $fields
      ->addColumn( 'other_title', 'string', [ 'limit' => 255, 'default' => null, 'null' => true])
      ->save();  
  }
}
