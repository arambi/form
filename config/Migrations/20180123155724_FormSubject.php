<?php
use Migrations\AbstractMigration;

class FormSubject extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $forms = $this->table( 'form_forms');
    $forms
      ->addColumn( 'has_subject', 'boolean', ['default' => null, 'null' => true])
      ->addColumn( 'subject', 'string', ['default' => null, 'null' => true])
      ->update();

    $forms_translations = $this->table( 'form_forms_translations');
    $forms_translations
      ->addColumn( 'subject', 'string', ['default' => null, 'null' => true])
      ->update();  
  }
}
