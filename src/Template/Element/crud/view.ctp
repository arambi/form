  <div class="row wrapper wrapper-update border-bottom white-bg page-heading">
    <div class="col-lg-12 header-content">
      <h2><i class="{{ data.crudConfig.navigation.icon }}"></i> <?= __d( 'admin', 'Respuesta al formulario {{ data.content.form.title }}') ?>
        <span ng-if="data.crudConfig.view.actionButtons" class="btn btn-default btn-xs cf-action-button" ng-repeat="action in data.crudConfig.view.actionButtons"><a href="{{ action.url }}">{{ action.label }}</a></span>
      </h2>
    </div>
    <div class="col-lg-2"></div>
  </div>
  <div class="animated fadeIn">
    <div ng-init="crudConfig = data.crudConfig">
      <ng-include src="'Manager/_warning.html'"></ng-include>

      <!-- Resumen -->
      <div class="ibox">
        <div class="ibox-content">
          <div class="row">
            <div class="col-md-12">
              <h4><?= __d( 'admin', 'Recibido el {0}', [
                '{{ data.content.created | date }}'
              ]) ?></h4>
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example">
                  <tbody ng-if="data.content.html_admin_extra" ng-bind-html="data.content.html_admin_extra"></tbody>
                  <tr ng-repeat="response_field in data.content.response_fields">
                    <td>{{ response_field.field.title }}</td>
                    <td ng-if="response_field.field.type != 'file'">{{ response_field.value_human }}</td>
                    <td ng-if="response_field.field.type == 'file'">
                      <span ng-repeat="file in response_field.value"><a download href="{{ file.paths[0] }}">{{ file.filename }}</a></span>
                    </td>
                  </tr>
                  <!-- <tr ng-if="data.content.uri_referer">
                    <td>URL desde donde se envió</td>
                    <td><a target="blank" href="{{ data.content.uri_referer }}">{{ data.content.uri_referer }}</a></td>
                  </tr> -->
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>