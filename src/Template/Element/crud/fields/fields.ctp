<div class="row">
  <div class="col-md-12 cf-form-buttons">
    <button cf-form-add-field="text" tooltip="<?= __d('admin', 'Añadir campo de texto') ?>" class="btn btn-primary btn-circle btn-lg"><i class="fa fa-font"></i></button>
    <button cf-form-add-field="select" tooltip="<?= __d('admin', 'Añadir campo de lista de opciones') ?>" class="btn btn-primary btn-circle btn-lg"><i class="fa fa-chevron-down"></i></button>
    <button cf-form-add-field="checkbox" tooltip="<?= __d('admin', 'Añadir campo de casillas de selección') ?>" class="btn btn-primary btn-circle btn-lg"><i class="fa fa-check-square-o"></i></button>
    <button cf-form-add-field="radio" tooltip="<?= __d('admin', 'Añadir campo de botones de radio') ?>" class="btn btn-primary btn-circle btn-lg"><i class="fa fa-dot-circle-o"></i></button>
    <button cf-form-add-field="boolean" tooltip="<?= __d('admin', 'Añadir campo casilla de verificación') ?>" class="btn btn-primary btn-circle btn-lg"><i class="fa fa-check-circle-o"></i></button>
    <button cf-form-add-field="textarea" tooltip="<?= __d('admin', 'Añadir campo de texto largo') ?>" class="btn btn-primary btn-circle btn-lg"><i class="fa fa-sticky-note-o"></i></button>
    <button cf-form-add-field="file" tooltip="<?= __d('admin', 'Añadir campo de archivo') ?>" class="btn btn-primary btn-circle btn-lg"><i class="fa fa-file-o"></i></button>
    <button cf-form-add-field="legend" tooltip="<?= __d('admin', 'Separador') ?>" class="btn btn-primary btn-circle btn-lg"><i class="fa fa-minus"></i></button>
    <button cf-form-add-field="body" tooltip="<?= __d('admin', 'Texto libre') ?>" class="btn btn-primary btn-circle btn-lg"><i class="fa fa-align-justify"></i></button>
  </div>
</div>
<div ui-sortable="{handle: '.handle-field',
              items: '.field-container',
              axis: 'y',
              distance: 20}" ng-model="content.fields" class="cf-form">
  <div ng-repeat="_field in content.fields" cf-form-fields ng-model="content.fields" class="row field-container block-container">
    <div class="col-md-12 cf-form-field">
      <div class="ibox float-e-margins cf-form-field">
        <div class="ibox-title handle-field">
          <h5 ng-click="_field.shows = !_field.shows">{{ data.crudConfig.view.fields.fields.crudConfigAdd.view.fields.type.options[_field.type] }} <small>{{ _field.title }}</small></h5>
          <div class="ibox-tools dropdown">
            <span tooltip="<?= __d('admin', 'Mover campo') ?>" class=""><i class="fa fa-arrows"></i></span>
            <span class="btn" tooltip="<?= __d('admin', 'Borrar') ?>" confirm-delete cf-destroy="scope.content.fields.splice( scope.$index, 1);"><i class="warning fa fa-trash"></i></span>
          </div>
        </div>
        <div class="ibox-content" ng-show="_field.shows">
          <div class="row" ng-if="_field.type !== 'body'">
            <div class="col-lg-6">
              <crud-field ng-init="crudConfig = data.crudConfig.view.fields.fields.crudConfigAdd; content = _field" field="title"></crud-field>
              <crud-field ng-if="_field.type != 'legend'" ng-init="crudConfig = data.crudConfig.view.fields.fields.crudConfigAdd; content = _field" field="placeholder"></crud-field>
              <crud-field ng-if="_field.type != 'legend'" ng-init="crudConfig = data.crudConfig.view.fields.fields.crudConfigAdd; content = _field" field="default_text"></crud-field>
            </div>
            <div class="col-lg-6">
              <crud-field ng-if="_field.type != 'legend'" ng-init="crudConfig = data.crudConfig.view.fields.fields.crudConfigAdd; content = _field" field="help_text"></crud-field>
              <crud-field ng-if="_field.type != 'legend'" ng-init="crudConfig = data.crudConfig.view.fields.fields.crudConfigAdd; content = _field" field="help_photo"></crud-field>
              <crud-field ng-if="_field.type != 'legend'" ng-init="crudConfig = data.crudConfig.view.fields.fields.crudConfigAdd; content = _field" field="required"></crud-field>
              <crud-field ng-if="_field.type != 'legend'" ng-if="_field.required == 1" ng-init="crudConfig = data.crudConfig.view.fields.fields.crudConfigAdd; content = _field" field="validation_method"></crud-field>
              <crud-field ng-init="crudConfig = data.crudConfig.view.fields.fields.crudConfigAdd; content = _field" field="class_name"></crud-field>
            </div>
          </div>
          <div class="row" ng-if="_field.type == 'body'">
            <div class="col-lg-12">
              <crud-field ng-init="crudConfig = data.crudConfig.view.fields.fields.crudConfigAdd; content = _field" field="body"></crud-field>
            </div>
          </div>
          <!-- begin @ opciones -->

          <div ng-if="_field.type != 'text' && _field.type != 'textarea' && _field.type != 'body' && _field.type != 'file' && _field.type != 'legend' && _field.type != 'boolean'" class="cf-form-options">
            <h4><?= __d('admin', 'Opciones') ?></h4>
            <a cf-form-add-option ng-model="_field" href class="btn btn-outline btn-default btn-xs m-b"><i class="fa fa-plus"></i> <span class="bold"><?= __d('admin', 'Añadir opción') ?></span></a>
            <div ui-sortable="sortableOptions" ng-model="_field.options">
              <div ng-repeat="option in _field.options" cf-form-options ng-model="_field.options" class="row cf-form-option">
                <div class="col-md-6">
                  <div class="row">
                    <div class="col-md-8">
                      <crud-field ng-init="crudConfig = data.crudConfig.view.fields.fields.crudConfigAdd.view.fields.options.crudConfigAdd; content = option" field="title"></crud-field>
                    </div>
                    <div class="col-md-4">
                      <crud-field ng-if="_field.type == 'checkbox' || _field.type == 'radio'" ng-init="crudConfig = data.crudConfig.view.fields.fields.crudConfigAdd.view.fields.options.crudConfigAdd; content = option" field="other"></crud-field>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-11 col-md-push-1">
                      <crud-field ng-init="crudConfig = data.crudConfig.view.fields.fields.crudConfigAdd.view.fields.options.crudConfigAdd; content = option" field="other_title"></crud-field>
                    </div>
                  </div>
                </div>
                <div class="col-md-2">
                  <span tooltip="<?= __d('admin', 'Mover campo') ?>" class="handle handle-option"><i class="fa fa-arrows"></i></span>
                  <span class="btn" tooltip="<?= __d('admin', 'Borrar') ?>" confirm-delete cf-destroy="scope._field.options.splice( scope.$index, 1);"><i class="warning fa fa-trash"></i></span>
                </div>
              </div>
            </div>
          </div>
          <!-- end @ opciones -->

          <!-- begin @ mailchimp -->
          <?php if (\Cake\Core\Plugin::isLoaded('Mailchimp')) : ?>
            <div class="row cf-form-option">
              <div class="col-md-8">
                <crud-field ng-init="crudConfig = data.crudConfig.view.fields.fields.crudConfigAdd; content = _field" field="mailchimp_field_id"></crud-field>
              </div>
            </div>
            <div class="row cf-form-option">
              <div class="col-md-8">
                <crud-field ng-init="crudConfig = data.crudConfig.view.fields.fields.crudConfigAdd; content = _field" field="mailchimp_accept"></crud-field>
              </div>
            </div>
          <?php endif ?>
          <!-- end @ mailchimp -->
        </div>
      </div>
    </div>
  </div>
</div>