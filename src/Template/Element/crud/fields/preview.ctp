<div class="cf-form-preview">
  <div ng-repeat="_field in content.fields" class="form-group">
    <label class="cf-form-preview-label">{{ _field._translations[data.currentLanguage.iso3].title }} <span ng-if="_field.required">(*)</span></label>
    <input class="form-control" ng-if="_field.type == 'text'" type="text" value="{{ _field[currentLanguage.iso3].default_text }}" />
    <span ng-if="_field[data.currentLanguage.iso3].help_text" class="help-block m-b-none">{{ _field._translations[data.currentLanguage.iso3].help_text }}</span>

    <select class="form-control" ng-if="_field.type == 'select'">
      <option ng-repeat="option in _field.options">{{ option._translations[data.currentLanguage.iso3].title }}</option>
    </select>

    <div ng-if="_field.type == 'checkbox'" ng-repeat="option in _field.options">
      <label><input type="checkbox" /> {{ option._translations[data.currentLanguage.iso3].title }}</label>
    </div>

    <div ng-if="_field.type == 'radio'" ng-repeat="option in _field.options">
      <label><input type="radio" /> {{ option._translations[data.currentLanguage.iso3].title }}</label>
    </div>

    <div ng-if="_field.type == 'textarea'">
      <label>{{ option._translations[data.currentLanguage.iso3].title }}</label>
      <textarea class="form-control" rows="5"></textarea>
    </div>
  </div>
</div>