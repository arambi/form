<?php

namespace Form\View\Helper;

use Cake\View\View;
use Cake\View\Helper;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use I18n\Lib\Lang;

/**
 * Formulary helper
 */
class FormularyHelper extends Helper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    private $options = [];

    public $helpers = [
        'Form',
        'Html',
        'Cofree.Buffer',
        'Section.Nav',
        'Recaptcha.Recaptcha'
    ];

    private $hasForm = false;
    private $hasUpload = false;

    protected $_isLegendOpen = false;

    public function beforeRender()
    {
        $this->messages = [
            'required' => __d('app', 'Este campo es obligatorio'),
            'email' => __d('app', 'Debe ser un email válido'),
            'number' => __d('app', 'Este campo debe ser un número'),
        ];

        $this->Form->addWidget('radio', ['\Form\View\Widget\RadioWidget', 'nestingLabel']);
    }

    public function beforeLayout()
    {
        if (!$this->request->getParam('prefix') || $this->request->getParam('prefix') != 'admin') {
            $this->Html->css([
                '/cofree/loadingimagethingy/jquery.loadingimagethingy.css'
            ], [
                'block' => true
            ]);
        }
    }

    /**
     * Devuelve un formulario
     * 
     * @param  Form\Model\Entity\Form $form
     * @return string HTML
     */
    public function form($form, $options = [])
    {
        $this->options = $options;
        $this->hasForm = true;
        $id = 'form-' . rand(1111, 9999);
        $div = 'wrapper-' . rand(1111, 9999);
        $out = [];

        if ($form->show_title) {
            $out[] = '<h2>' . $form->title . '</h2>';
        }

        if (!empty($form->body)) {
            $out[] = '<div class="b-text form-intro">' . $form->body . '</div>';
        }

        $out[] = $this->Form->create(null, [
            'id' => $id,
            'url' => '/'. Lang::current('iso2') .'/form/forms/add.json',
        ]);

        $out[] = $this->Form->hidden('Responses.salt', [
            'value' => $form->salt
        ]);

        $out[] = $this->Form->hidden('Responses.uri_referer', [
            'value' => Router::url(env('REQUEST_URI'), true)
        ]);

        if (isset($options['block']) && !empty($options['block']->extra)) {
            $out[] = '<input type="hidden" name="Responses[extra]" value=\'' . json_encode($options['block']->extra) . '\'>';
        }

        $fields = [];

        foreach ((array)$form->fields as $field) {
            $fields[] = $this->field($field);
        }

        if ($this->_isLegendOpen) {
            $fields[] = '</fieldset>';
        }

        $out[] = '<div class="form-fields">' . implode("\n", $fields) . '</div>';

        $text = [];
        $text[] = $this->acceptText($form);
        $text[] = $this->Html->tag('p', __d('app', '(*) Campos obligatorios'), [
            'class' => 'form-mandatory-text'
        ]);

        $bottom = [];

        $bottom[] = $this->Html->tag('div', implode("\n", $text), [
            'class' => 'form-notice'
        ]);

        if (!empty($form->legal_text)) {
          $bottom[] =  '<div class="form-notice form-legal-text">' . $form->legal_text . '</div>';
        }

        if (!Configure::read('Recaptcha.version') == 'invisible') {
            $recaptcha = [
                $this->Recaptcha->display(),
                '<div class="recaptcha-error"></div>'
            ];

            $bottom[] =  '<div class="recaptcha-containter">' . implode("\n", $recaptcha) . '</div>';
        }

        

        $out[] = '<div class="form-bottom">' . implode("\n", $bottom) . '</div>';

        $out[] = '<div class="form-submit">' . $this->Form->button($form->submit_text) . '</div>';

        if (Configure::read('Recaptcha.version') == 'invisible') {
            $out[] = '<div class="g-recaptcha"
          data-sitekey="' .  Configure::read('Recaptcha.siteKey') . '"
          data-callback="submitRecaptcha"
          data-size="invisible">
      </div>';
            $out[] = $this->Html->scriptBlock('function submitRecaptcha(){ console.log("recapcha")}');
        }


        $out[] = $this->Form->end();
        $this->script($div, $id, $form);

        return $this->Html->tag('div', implode("\n", $out), [
            'id' => $div,
            'class' => 'form-wrapper'
        ]);
    }

    public function script($div, $id, $form)
    {
        $this->Buffer->start();
        echo '<script src="/form/js/validate/jquery.validate.js"></script>';
        echo '<script src="/cofree/loadingimagethingy/jquery.loadingimagethingy.js"></script>';

        if ($this->hasUpload) {
            echo $this->Html->script([
                'http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js',
                '/manager/js/plupload/2.3.1/js/plupload.full.min.js',
                '/manager/js/plupload/2.3.1/js/jquery.ui.plupload/jquery.ui.plupload.min.js',
            ]);
        }

        if (Configure::read('Recaptcha.version') == 'invisible') {
            $recapchaCode = 'grecaptcha.execute();';
        } else {
            $recapchaCode = '';
        }

        $beforeSend = isset($this->options['beforeSend']) ? $this->options['beforeSend'] : '';

        $script = <<<EOF
    (function(){
      function observeFormOther( el){
        var input = '.' + el.data( 'divclass');
        if( el.is( ':checked')){
          $(input).show();
        } else {
          $(input).hide();
        } 
      };

      $("#$div input[type=radio]").change( function(){
        $('.form-other').each(function(){
           observeFormOther( $(this).find( 'input'));
        });
      })

      $('.form-other').each(function(){
        $(this).find( 'input')
          .data( 'divclass', $(this).data( 'divclass'))
          .change(function(){
            observeFormOther( $(this));
          });
         observeFormOther( $(this).find( 'input'));
      });

      var div = $("#$div");
      $("#$id").validate({
        submitHandler: function(form) {
          $recapchaCode
          $.ajax({
            type: 'post',
            url: $("#$id").attr( 'action'),
            beforeSend: function(){
              div.loadingimagethingy( 'enable');
            },
            success: function( data){
              if( data && data.recaptchaError){
                $(".recaptcha-error").html( data.recaptchaError);
                div.loadingimagethingy( 'disable');
              } else {
                $("#$id").trigger('submited-form');
                $('body').trigger('formSended', [{
                  cname: '{$form->cname}',
                  data: $("#$id").serialize()
                }]);

                if( data && data.redirect){
                  window.location.href = data.redirect;
                } else if( data.text) {
                  div.html( '<div class="form-response-success">' + data.text + '</div>');
                }
                
                $beforeSend
                div.loadingimagethingy( 'disable');
              }
                
            },
            error: function( data){
              console.log( data);
            },
            data: $("#$id").serialize()
          });

          return false;
        }
       });
    }())
EOF;
        echo $this->Html->scriptBlock($script);
        $this->Buffer->end();
    }

    public function field($field)
    {
        if ($field->type == 'body') {
            return '<div class="field-body">' . $field->body . '</div>';
        }

        $method = '_field' . ucfirst($field->type);

        $label = $field->title;

        if ($field->required) {
            $label .= ' (*)';
        }

        $type = $field->type;

        if ($type == 'checkbox') {
            $type = 'multiCheckbox';
        }

        if ($type == 'boolean') {
            $type = 'checkbox';
        }

        $options = [
            'type' => $type,
            'label' => $field->placeholder ? false : $label,
            'placeholder' => !$field->placeholder ? false : $label,
        ];

        if (!empty($field->class_name)) {
            $options['templateVars']['div_class'] = $field->class_name;
        }

        if (!empty($field->options)) {
            $_options = [];

            foreach ($field->options as $option) {
                $text = $option->title;
                $templateVars = [];

                if (!empty($option->other)) {
                    $templateVars['after'] = $this->Form->input('Responses.' . $field->id . '_other_text_' . $option->id, [
                        'label' => !empty($option->other_title) ? $option->other_title : false,
                        'type' => 'text',
                        'templateVars' => [
                            'div_class' => 'form-other-text form-other-text-' . $option->id
                        ],
                    ]);

                    $templateVars['class'] = 'form-other';
                    $templateVars['attrs'] = 'data-divclass="form-other-text-' . $option->id . '"';
                }

                $_options[] = [
                    'value' => $option->id,
                    'text' => $text,
                    'templateVars' => $templateVars,
                ];
            }

            $options['options'] = $_options;
        }

        if ($field->required) {
            $options['required'] = 1;
            $options[$field->validation_method] = 1;
            $options['data-msg'] = isset($this->messages[$field->validation_method]) ? $this->messages[$field->validation_method] : __d('app', 'Este campo es obligatorio');
        }

        if (method_exists($this, $method)) {
            return $this->$method($field, $options);
        }

        return $this->Form->input('Responses.' . $field->id, $options);
    }

    protected function _fieldLegend($field)
    {
        if ($this->_isLegendOpen) {
            $return[] = '</div></fieldset>';
        }

        $this->_isLegendOpen = true;
        $return[] = '<fieldset class="' . $field->class_name . '"><legend>' . $field->title . '</legend><div class="fieldset-fields">';

        return implode("\n", $return);
    }

    protected function _fieldFile($field, $options)
    {
        $this->hasUpload = true;

        $this->Buffer->scriptStart();
        $url = "/upload/Uploads/upload.json?key=doc";
        $id = 'Responses_' . $field->id;
        $button_id = 'uploader_' . $field->id;
        $script = <<<EOF
    $(function() {
      var uploader = new plupload.Uploader({
        runtimes : 'html5,html4',
        browse_button : '$button_id', // you can pass in id...
        url : "$url",
        filters : {
          max_file_size : '10mb',
          mime_types: [
              {title : "Image files", extensions : "jpg,gif,png,pdf,doc,docx"},
              {title : "Zip files", extensions : "zip"}
          ]
        },
        file_data_name: 'filename',
        init: {
          PostInit: function() {
            
          },
          FilesAdded: function(up, files) {
            plupload.each(files, function(file) {
              uploader.start();
            });
            update();
          },
          FileUploaded: function( up, file, result){
            var response = JSON.parse( result.response);
            if( response.success){
              for( i in up.files){
                if( up.files[i].id == file.id){
                  up.files[i].asset = response.upload;
                }
              }
            }

            update();
          },
          UploadProgress: function(up, file) {
              update();
          },
          Error: function(up, err) {
              
          }
        }
    });
     
    uploader.init();


    function update(){
      var html = '';
      var data = [];
      var files = uploader.files;
      for( i in files){
        if( files[i].status == 5){
          html += '<span>' + files[i].name + '<span data-id="'+ files[i].id +'"><i class="fa fa-remove"></i></span></span>';
          data.push(files[i].asset);
        } else if( files[i].status == 2){
          html += '<span><i class="fa fa-circle-o-notch fa-spin fa-fw"></i> '+ files[i].name +'</span>'
        } else if( files[i].status == 4){
          html += '<span><i class="fa fa-warning"></i> '+ files[i].name +'</span>'
        }
      }

      $("#uploads_$id").html( html);
      $('#$id').val( JSON.stringify(data));
    }

    $("#uploads_$id").delegate( 'span span', 'click', function(){
      uploader.removeFile( $(this).data( 'id'));
      update();
    })
});
EOF;
        echo $this->Html->scriptBlock($script);
        $this->Buffer->scriptEnd();
        $content = [];
        $content[] = $this->Form->input('Responses.' . $field->id, [
            'type' => 'hidden',
            'id' => $id
        ]);

        $content[] = '<label class="label">' . $field->title . '</label>';
        $content[] = '<div class="form-upload"><span class="form-button-upload" id="' . $button_id . '">' . __d('app', 'Subir fichero') . '</span></div>';
        $content[] = '<div class="form-uploads" id="uploads_' . $id . '"></div>';

        if (!empty($field->help_text)) {
          $content[] = '<div class="form-help">'. $field->help_text . '</div>';
        }

        return $this->Form->formatTemplate('inputContainer', [
            'content' => implode("\n", $content),
            'label' => $field->title,
            'type' => 'upload',
            'templateVars' => isset($options['options']['templateVars']) ? $options['options']['templateVars'] : []
        ]);
    }

    public function acceptText($form)
    {
        if ($form->custom_privacy && !empty(strip_tags($form->privacy_text))) {
            $text = $form->privacy_text;
        } else if (!empty($form->privacy_section_id)) {
            $url = TableRegistry::getTableLocator()->get('Section.Sections')->getUrl($form->privacy_section_id);
            $text = __d('app', 'He leído y acepto la {0} política de protección de datos {1}', [
                '<a href="' . $url . '" target="_blank">',
                '</a>',
            ]) . ' *';
        } else {
            return;
        }

        return $this->Form->input('accept', [
            'label' => [
                'text' => strip_tags($text, '<a>'),
                'param-button' => '',
                'escape' => false
            ],
            'id' => 'input-'. rand(0, 9999999),
            'type' => 'checkbox',
            'required' => true,
            'class' => 'required',
            'escape' => false,
            'title' => __d('app', 'Es necesario marcar esta casilla')
        ]);
    }
}
