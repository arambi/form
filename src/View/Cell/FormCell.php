<?php

namespace Form\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * Form cell
 */
class FormCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($block)
    {
        $query = TableRegistry::getTableLocator()->get('Form.Forms')->find();

        $query->contain([
            'Fields' => [
                'Options'
            ]
        ]);

        if (!empty($block->cname)) {
            $query->where(['Forms.cname' => $block->cname]);
        } else {
            $query->where(['Forms.id' => $block->parent_id]);
        }

        $form = $query->first();
        $this->set(compact('form', 'block'));
    }
}
