<?php
namespace Form\Controller\Admin;

use Cake\Core\Plugin;
use Website\Lib\Website;
use Form\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * Forms Controller
 *
 * @property \Form\Model\Table\FormsTable $Forms
 */
class FormsController extends AppController
{
  use CrudControllerTrait;

  protected function _update( $content)
  {
    if( Plugin::isLoaded( 'Mailchimp') && Website::get( 'settings.mailchimp'))
    {
      if( !empty( $content->mailchimp_list_id))
      {
        try {
          $mc = new \DrewM\MailChimp\MailChimp( Website::get( 'settings.mailchimp'));
          $list =   $mc->get( 'lists/'. $content->mailchimp_list_id .'/merge-fields');

          if( empty( $list ['merge_fields']))
          {
            return;
          }

          $fields = collection( $list ['merge_fields'])->combine( 'tag', 'name')->toArray();
          $this->CrudTool->addSerialized([
            'mc_fields' => $fields,
        ]);
        } catch (\Throwable $th) {
          //throw $th;
        }
      }
    }
  }
}
