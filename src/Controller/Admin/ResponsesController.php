<?php
namespace Form\Controller\Admin;

use Form\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * Responses Controller
 *
 * @property \Form\Model\Table\ResponsesTable $Responses
 */
class ResponsesController extends AppController
{
  use CrudControllerTrait {
    initialize as initializeCrud;
  }

  public function initialize()
  {
    $this->initializeCrud();
    
    if( $this->request->param( 'action') == 'index' && !$this->request->param( 'pass'))
    {
      $this->loadModel( 'Form.Forms');      
      $this->modelClass ='Form.Forms';
    }
  }

  public function beforeFilter( \Cake\Event\Event $event)
  {
    if( $this->request->param( 'action') == 'index')
    {
      if( $this->request->param( 'pass'))
      {
        $form = $this->Responses->Forms->get( $this->request->param( 'pass')[0]);

        $index_fields = [];
        
        foreach( $form->fields as $field)
        {
          $index_fields ['field_'. $field->id] = [
            'type' => 'string',
            'label' => $field->title
          ];
        }

        $index_fields ['created'] = [
          'label' => __d( 'admin', 'Fecha'),
          'type' => 'date'
        ];

        $this->Responses->crud
          ->addFields( $index_fields)
          ->addIndex( 'index', [
            'fields' => $index_fields,
            'actionButtons' => [
              'index'
            ],
            'saveButton' => false,
            'exportCsv' => true,
            'csvFields' => collection( $index_fields)->extract( 'label')->toArray()
          ]);
      }
      else 
      {
        $this->Forms->crud->adminUrl( '/admin/form/responses/');
        $this->Forms->crud
          ->addFields([
            'title' => __d( 'admin', 'Título'),
          ])
          ->addIndex( 'index', [
            'fields' => [
              'title',
            ],
            'actionLink' => 'index',
            'actionButtons' => ['create'],
            'saveButton' => false,
          ]);
      }
    }
  }

/**
 * Modificación de los campos del index
 * Pone los campos del formulario y las respuestas
 * 
 * @param  Cake\ORM\Query $query
 * @return
 */
  protected function _index( $query)
  {
    if( $this->request->param( 'pass'))
    {
      $query->where([
        'Responses.form_id' => $this->request->param( 'pass')[0]
      ]);
      
      $query->group([
        'Responses.id'
      ]);
      
      $_query = $this->request->query;

      if( !empty( $_query))
      {
        $this->Responses->hasOne( 'ResponseField', [
          'foreignKey' => 'response_id',
          'className' => 'Form.ResponseFields'
        ]);

        $this->Responses->crud->addAssociations( ['ResponseField']);

        foreach( $_query as $key => $value)
        {
          if( strpos( $key, 'field_') !== false)
          {
            list( , $field_id) = explode( '_', $key);

            $query->andWhere([
              'ResponseField.field_id' => $field_id,
              'ResponseField.value LIKE' => "%$value%"
            ]);

            unset( $this->request->query [$key]);
          }
        }
      }

      $query->formatResults( function( $results){
        return $results->map( function( $row){
          foreach( $row->response_fields as $response_field)
          {

            $property = 'field_'. $response_field->field->id;
            $value = $response_field->value_string;
            $row->$property = $value;
          }

          return $row;
        });
      });
    }
      
  }

  public function view( $id)
  {

  }
}
