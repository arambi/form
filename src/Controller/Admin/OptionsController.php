<?php
namespace Form\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Form\Controller\AppController;

/**
 * Options Controller
 *
 * @property \Form\Model\Table\OptionsTable $Options
 */
class OptionsController extends AppController
{
    use CrudControllerTrait;
}
