<?php
namespace Form\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Form\Controller\AppController;

/**
 * Fields Controller
 *
 * @property \Form\Model\Table\FieldsTable $Fields
 */
class FieldsController extends AppController
{
    use CrudControllerTrait;
}
