<?php

namespace Form\Controller;

use Cake\Core\App;
use Cake\Core\Plugin;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Form\Controller\AppController;
use Letter\Mailer\MailerAwareTrait;

/**
 * Forms Controller
 *
 * @property \Form\Model\Table\FormsTable $Forms
 */
class FormsController extends AppController
{
    use MailerAwareTrait;

    public function initialize()
    {
        parent::initialize();

        if (isset($this->Auth)) {
            $this->Auth->allow();
        }

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Cofree.Recaptcha');
    }

    public function add()
    {
        $this->loadModel('Form.Responses');
        $this->set('_serialize', true);

        if (Configure::read('Recaptcha.version') != 'invisible') {
            $verify = $this->Recaptcha->verify();
        }

        if ($verify) {
            $response = $this->Responses->build($this->request);

            if ($this->__isSpamer($response->email)) {
                $this->__responseAction($response);
                return;
            }

            if ($this->Responses->save($response)) {
                $response = $this->Responses->find()
                    ->where(['Responses.id' => $response->id])
                    ->contain(['Forms', 'ResponseFields' => ['Fields']])
                    ->first();

                $this->__afterSave($response);

                $event = new Event('Form.Controller.Forms.afterSend', $this, [
                    $response,
                ]);

                $this->getEventManager()->dispatch($event);
            }
        }
    }

    private function __isSpamer($email)
    {
        $path = Plugin::path('Form') . 'src/Files/spamers.txt';
        $contents = file_get_contents($path);
        $emails = explode("\n", $contents);
        return in_array($email, $emails);
    }

    private function __afterSave($response)
    {
        // Envio del correo electrónico
        $emails = explode(',', $response->form->email_response);

        foreach ($emails as $email) {
            $this->getMailer('Form.Form')->send('add', [$response, trim($email)]);
        }

        if ($response->email) {
            $this->getMailer('Form.Form')->send('client', [$response]);
        }

        $this->__responseAction($response);
    }

    private function __responseAction($response)
    {
        if ($response->form->after_finish_option == 'redirect') {
            $section_id = $response->form->after_finish_section_id;
            $this->loadModel('Section.Sections');
            $url = $this->Sections->getUrl($section_id);
            $this->set('redirect', Router::url($url, true));
        } elseif ($response->form->after_finish_option == 'text') {
            $this->set('text', $response->form->after_finish_text);
        }
    }
}
