# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2017-02-03 07:00+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: /View/Helper/FormularyHelper.php:33;193
msgid "Este campo es obligatorio"
msgstr ""

#: /View/Helper/FormularyHelper.php:34
msgid "Debe ser un email válido"
msgstr ""

#: /View/Helper/FormularyHelper.php:35
msgid "Este campo debe ser un número"
msgstr ""

#: /View/Helper/FormularyHelper.php:100
msgid "(*) Campos obligatorios"
msgstr ""

#: /View/Helper/FormularyHelper.php:208
msgid "Al enviar, acepto la {0} política de privacidad {1}"
msgstr ""

