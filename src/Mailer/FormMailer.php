<?php

namespace Form\Mailer;

use Cake\Mailer\Mailer;
use Letter\Collector\LetterCollector;
use Letter\Mailer\LetterTrait;
use Website\Lib\Website;
use Letter\Mailer\QueueMailer;


/**
 * Form mailer.
 */
class FormMailer extends QueueMailer
{
    use LetterTrait;

    /**
     * Mailer's name.
     *
     * @var string
     */
    static public $name = 'Form';

    public function add($response, $email)
    {
        $this->letter = LetterCollector::get('Form.new', [
            'web_name' => Website::get('title'),
            'form_name' => $response->form->title,
            'data' => $response->toHtml()
        ]);


        $this->_setLetterParams();

        if ($response->form->has_subject && !empty($response->form->subject)) {
            $this->_email->setSubject($response->form->subject);
        }

        if ($response->form->custom_email_from_bool && !empty($response->form->custom_email_from)) {
            $emailFrom = $response->form->custom_email_from;
        } else {
            $emailFrom = !empty(Website::get('settings.users_reply_email')) ? Website::get('settings.users_reply_email') : Website::get('email');
        }

        $this->_email->setFrom($emailFrom, Website::get('title'));
        $this->_email->setTo($email);
    }

    public function client($response)
    {
        $this->letter = LetterCollector::get('Form.new_user', [
            'web_name' => Website::get('title'),
            'form_name' => $response->form->title,
            'data' => $response->toHtml()
        ]);

        $this->_setLetterParams();

        if ($response->form->custom_mail_body && !empty($response->form->mail_body)) {
            $this->_email->set('body', $response->form->mail_body);
        }

        if ($response->form->custom_email_from_bool && !empty($response->form->custom_email_from)) {
            $email = $response->form->custom_email_from;
        } else {
            $email = !empty(Website::get('settings.users_reply_email')) ? Website::get('settings.users_reply_email') : Website::get('email');
        }

        $this->_email->setFrom($email, Website::get('title'));
        $this->_email->setTo($response->email);
    }
}
