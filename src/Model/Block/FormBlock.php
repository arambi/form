<?php

namespace Form\Model\Block;
use Cake\ORM\TableRegistry;

class FormBlock
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Formulario'),
          'plural' => __d( 'admin', 'Formulario'),
        ])
      ->addFields([
        'parent_id' => [
          'label' => __d( 'admin', 'Formulario'),
          'type' => 'select',
          'options' => function( $crud){
            $return = TableRegistry::get( 'Form.Forms')->find( 'list')->toArray();
            return $return;
          }
        ],
        'key' => [
          'type' => 'hidden'
        ]
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'parent_id',
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}