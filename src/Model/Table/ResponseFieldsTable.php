<?php
namespace Form\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Form\Model\Entity\ResponseField;

/**
 * ResponseFields Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Responses
 * @property \Cake\ORM\Association\BelongsTo $Fields
 */
class ResponseFieldsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table( 'form_response_fields');
        $this->displayField( 'id');
        $this->primaryKey( 'id');

        $this->addBehavior( 'Timestamp');

        $this->belongsTo( 'Responses', [
            'foreignKey' => 'response_id',
            'className' => 'Form.Responses'
        ]);

        $this->belongsTo( 'Fields', [
            'foreignKey' => 'field_id',
            'className' => 'Form.Fields'
        ]);
    }

}
