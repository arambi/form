<?php
namespace Form\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Form\Model\Entity\Option;
use Cake\Core\Configure;

/**
 * Options Model
 */
class OptionsTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table( 'form_options');
    $this->displayField( 'title');
    $this->primaryKey( 'id');
    $this->addBehavior( 'Timestamp');
    
    // Behaviors
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');
    $this->addBehavior( Configure::read( 'I18n.behavior'), [
      'fields' => ['title', 'other_title'],
      'translationTable' => 'Form.FormsI18n',
    ]);

    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    // $this->crud->associations([]);

    $this->crud
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'other' => [
          'label' => __d( 'admin', 'Otro'),
          'help' => __d( 'admin', 'Indica esta opción para que el usuario introduzca un campo de texto al seleccionar esta opción')
        ],
        'other_title' => [
          'label' => __d( 'admin', 'Texto'),
          'show' => 'content.other'
        ],
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title',
          
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Opciones'),
        'plural' => __d( 'admin', 'Opciones'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'title',
                  'other',
                  'other_title',
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => ['create', 'index']
      ], ['update'])
      ;
    
  }
}
