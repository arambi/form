<?php

namespace Form\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Core\Plugin;
use Cake\Core\Configure;
use Website\Lib\Website;
use Cake\ORM\RulesChecker;
use Form\Model\Entity\Field;
use Cake\Validation\Validator;

/**
 * Fields Model
 */
class FieldsTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table('form_fields');
    $this->displayField('title');
    $this->primaryKey('id');

    // Associations
    $this->hasMany('Options', [
      'alias' => 'Options',
      'foreignKey' => 'field_id',
      'className' => 'Form.Options',
      'sort' => ['Options.position'],
      // 'dependent' => true
    ]);

    // Behaviors
    $this->addBehavior('Timestamp');
    $this->addBehavior('Manager.Crudable');
    $this->addBehavior('Cofree.Saltable');
    $this->addBehavior(Configure::read('I18n.behavior'), [
      'fields' => ['title', 'help_text', 'default_text', 'body'],
      'translationTable' => 'Form.FormsI18n',
    ]);

    $this->addBehavior('Upload.UploadJsonable', [
      'fields' => [
        'help_photo'
      ]
    ]);

    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    $this->crud->addAssociations(['Options']);

    $this->crud
      ->addFields([
        'title' => [
          'label' => __d('admin', 'Título'),
          'help' => __d('admin', 'Es el nombre de referencia que aparecerá junto al campo')
        ],
        'body' => [
          'label' => __d('admin', 'Texto'),
          'help' => __d('admin', 'Texto libre')
        ],
        'help_text' => [
          'label' => __d('admin', 'Ayuda'),
          'help' => __d('admin', 'Texto de ayuda para el usuario que aparecerá junto al campo')
        ],
        'help_photo' => [
          'label' => __d('admin', 'Imagen de ayuda'),
          'help' => __d('admin', 'Imagen de ayuda para el usuario que aparecerá junto al campo'),
          'type' => 'upload',
          'config' => [
            'type' => 'default',
            'size' => 'thm'
          ]
        ],
        'default_text' => [
          'label' => __d('admin', 'Por defecto'),
          'help' => __d('admin', 'Texto por defecto que aparecerá en el campo.'),
          'show' => 'content.type != "boolean"'
        ],
        'required' => [
          'label' => __d('admin', 'Obligatorio'),
          'help' => __d('admin', 'Al marcar esta opción será obligatorio rellenar el campo'),
          'show' => 'content.type != "boolean"'
        ],
        'validation_method' => [
          'label' => __d('admin', 'Método'),
          'help' => __d('admin', 'Selecciona el método de validación'),
          'type' => 'select',
          'options' => [
            'required' => __d('admin', 'No vacío'),
            'email' => __d('admin', 'Dirección de email'),
            'url' => __d('admin', 'Una URL'),
            'number' => __d('admin', 'Un número'),
          ],
          'show' => 'content.required'
        ],
        'placeholder' => [
          'label' => __d('admin', 'Dentro del campo'),
          'help' => __d('admin', 'Al marcar esta opción el título del campo aparecerá dentro del campo')
        ],
        'options' => [
          'label' => __d('admin', 'Campos'),
          'type' => 'hasMany',
        ],
        'type' => [
          'options' => [
            'text' => __d('admin', 'Texto'),
            'select' => __d('admin', 'Lista'),
            'checkbox' => __d('admin', 'Casillas de opciones'),
            'radio' => __d('admin', 'Opciones de radio'),
            'textarea' => __d('admin', 'Campo de texto largo'),
            'file' => __d('admin', 'Fichero adjunto'),
            'boolean' => __d('admin', 'Casilla de verificación'),
            'legend' => __d('admin', 'Separador'),
            'body' => __d('admin', 'Texto libre'),
          ]
        ],
        'class_name' => [
          'label' => __d('admin', 'Class de CSS'),
          'help' => __d('admin', 'Escribe aquí la class de CSS que tendrá el campo de input')
        ],
      ]);

    if (Plugin::isLoaded('Mailchimp') && !empty(Website::get('settings.mailchimp'))) {
      $this->crud->addFields([
        'mailchimp_field_id' => [
          'label' => __d('admin', 'Campo de Mailchimp'),
          'type' => 'select',
          'options' => function ($crud) {
            try {
              $content = $crud->getContent();
              $merge_fields = \Mailchimp\Store\MailchimpStore::getMergeFields($content['mailchimp_list_id']);

              if (empty($merge_fields)) {
                return [];
              }

              $fields = [
                __d('app', '-- Ninguno --'),
                'email' => 'Email'
              ];

              return array_merge($fields, $merge_fields);
            } catch (\Throwable $th) {
              return [];
            }
          },
          'show' => 'data.content.has_mailchimp_list',
        ],
        'mailchimp_accept' => [
          'label' => __d('admin', 'Campo que acepta entrar en la lista de Mailchimp'),
          'show' => '_field.type == "boolean" && data.content.has_mailchimp_list'
        ]
      ]);
    }


    $this->crud->addIndex('index', [
      'fields' => [
        'title',
      ],
      'actionButtons' => ['create'],
      'saveButton' => false,
    ])
      ->setName([
        'singular' => __d('admin', 'Campos'),
        'plural' => __d('admin', 'Campos'),
      ])
      ->addView('create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'title',
                  'help_text',
                  'help_photo',
                  'default_text',
                  'required',
                  'placeholder',
                  'options'
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => ['create', 'index']
      ], ['update']);
  }
}
