<?php

namespace Form\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\RulesChecker;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Form\Model\Entity\Response;
use Cake\Database\Schema\TableSchema;

/**
 * Responses Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Forms
 */
class ResponsesTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('form_responses');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Forms', [
            'foreignKey' => 'form_id',
            'className' => 'Form.Forms'
        ]);

        $this->hasMany('ResponseFields', [
            'foreignKey' => 'response_id',
            'className' => 'Form.ResponseFields'
        ]);

        $this->addBehavior('Timestamp');

        $this->addBehavior('Cofree.Saltable');

        $this->addBehavior('Manager.Crudable');

        // CRUD Config
        $this->crud->associations(['ResponseFields', 'Fields']);

        $this->crud
            ->addFields([
                'created' => __d('admin', 'Fecha'),

            ]);

        $this->crud
            ->setName([
                'singular' => __d('admin', 'Respuesta'),
                'plural' => __d('admin', 'Respuestas'),
            ])
            ->addView('update', [
                'template' => 'Form/view',
                'columns' => [
                    [
                        'title' => __d('admin', 'Contenido'),
                        'cols' => 8,
                        'box' => [
                            [
                                'elements' => []
                            ]
                        ],

                    ],

                ],
                'actionButtons' => [
                    function ($crud) {
                        return [
                            'label' => 'Listado',
                            'url' => '#/admin/form/responses/index/' . $crud->getContent()['form_id']
                        ];
                    }
                ]
            ]);


        $this->crud->order([
            'Responses.created' => 'desc'
        ]);
    }

    protected function _initializeSchema(TableSchema $schema)
    {
        $schema->setColumnType('extra', 'json');
        return $schema;
    }

    public function build($request)
    {
        $this->Fields = TableRegistry::getTableLocator()->get('Form.Fields');
        $data = $request->getData();

        $form = $this->Forms->find()
            ->where([
                'Forms.salt' => $data['Responses']['salt']
            ])
            ->first();

        unset($data['Responses']['salt']);
        $uri_referer = $data['Responses']['uri_referer'];
        unset($data['Responses']['uri_referer']);

        $extra = isset($data['Responses']['extra']) ? $data['Responses']['extra'] : null;
        unset($data['Responses']['extra']);

        if (array_key_exists('g-recaptcha-response', $data)) {
            unset($data['g-recaptcha-response']);
        }

        $response_fields = [];
        \Cake\Log\Log::debug($data['Responses']);
        foreach ($data['Responses'] as $field_id => $value) {
            if (strpos($field_id, 'other_text') !== false) {
                continue;
            }

            $response_fields[] = $this->ResponseFields->newEntity([
                'field_id' => $field_id,
                'value' => $this->value($value, $field_id, $data)
            ]);
        }

        $_data = $data['Responses'];
        $_data['ip'] = $request->clientIp();
        $_data['uri_referer'] = $uri_referer;

        if (!empty($extra)) {
            $_data['extra'] = json_decode($extra, true);
        }

        $entity = $this->newEntity($_data);
        $entity->set('form', $form);
        $entity->set('response_fields', $response_fields);
        return $entity;
    }


    public function value($value, $field_id, $data)
    {
        $field = $this->Fields->find()
            ->where([
                'Fields.id' => $field_id
            ])
            ->first();


        if (in_array($field->type, ['text', 'textarea', 'file', 'boolean'])) {
            return $value;
        }

        if ($field->type == 'checkbox') {
            $options = $this->Fields->Options->find('list')
                ->where(['field_id' => $field->id])
                ->toArray();

            $return = [];

            foreach ($value as $_val) {
                if (!is_array($_val) && isset($options[$_val])) {
                    $text = $options[$_val];

                    if (isset($data['Responses']["{$field_id}_other_text_{$_val}"])) {
                        $text .= ' (' . $data['Responses']["{$field_id}_other_text_{$_val}"] . ')';
                    }

                    $return[] = $text;
                }
            }

            return implode(', ', $return);
        }

        if ($field->type == 'radio') {
            $options = $this->Fields->Options->find('list')
                ->where(['field_id' => $field->id])
                ->toArray();

            if (isset($options[$value])) {
                $return = $options[$value];

                if (!empty(@$data['Responses']["{$field_id}_other_text_{$value}"])) {
                    $return .= ' (' . $data['Responses']["{$field_id}_other_text_{$value}"] . ')';
                }
            } else {
                $return = '';
            }

            return $return;
        }

        if ($field->type == 'select') {
            $options = $this->Fields->Options->find('list')
                ->where(['field_id' => $field->id])
                ->toArray();
        }

        return $options[$value];
    }
}
