<?php

namespace Form\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Core\Plugin;
use Cake\Event\Event;
use Cake\Core\Configure;
use Website\Lib\Website;
use Cake\ORM\RulesChecker;
use Cake\ORM\TableRegistry;
use Form\Model\Entity\Form;
use Cake\Validation\Validator;

/**
 * Forms Model
 */
class FormsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function Initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('form_forms');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        // Behaviors
        $this->addBehavior('Manager.Crudable');
        $this->addBehavior('Cofree.Saltable');
        $this->addBehavior(Configure::read('I18n.behavior'), [
            'fields' => ['title', 'after_finish_text', 'submit_text', 'subject', 'body', 'privacy_text', 'legal_text', 'mail_body'],
            'translationTable' => 'Form.FormsI18n',
        ]);

        $this->hasMany('Fields', [
            'alias' => 'Fields',
            'foreignKey' => 'form_id',
            'className' => 'Form.Fields',
            'sort' => ['Fields.position'],
            // 'dependent' => true
        ]);

        // CRUD Config
        //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
        $this->crud->addAssociations(['Fields', 'Options']);
        $this->crud->addJsFiles([
            '/form/js/directives.js',
        ]);

        $this->crud
            ->addFields([
                'title' => __d('admin', 'Título'),
                'body' => [
                    'label' => __d('admin', 'Texto introductorio'),
                    'help' => __d('admin', 'Texto que aparecerá al comienzo del formulario'),
                ],
                'show_title' => __d('admin', 'Mostrar título'),
                'has_subject' => [
                    'label' => __d('admin', 'Personalizar el tema del email al administrador'),
                    'help' => 'Marca esta opción si quieres que el tema del email que se envía al administrador no se un genérico sino que esté personalizado'
                ],
                'subject' => [
                    'label' => __d('admin', 'Tema del email'),
                    'show' => 'content.has_subject',
                ],
                'after_finish_option' => [
                    'label' => __d('admin', 'Después de enviar el formulario'),
                    'type' => 'select',
                    'options' => [
                        'redirect' => __d('admin', 'Redirigir a una página'),
                        'text' => __d('admin', 'Mostrar un texto')
                    ],
                    'help' => __d('admin', 'Una vez que el usuario ha enviado el formulario, selecciona la acción a realizar')
                ],
                'after_finish_text' => [
                    'label' =>  __d('admin', 'Texto'),
                    'help' => __d('admin', 'Texto que se mostrará una vez enviado el formulario'),
                    'show' => "content.after_finish_option == 'text'"
                ],
                'after_finish_section_id' => [
                    'label' => __d('admin', 'Pagina'),
                    'help' => __d('admin', 'Página a donde se redirigirá al usuario una vez enviado el formulario'),
                    'show' => "content.after_finish_option == 'redirect'",
                    'type' => 'select',
                    'options' => function ($crud) {
                        return TableRegistry::getTableLocator()->get('Section.Sections')->find('list')
                            ->where([
                                'Sections.action' => 'entry'
                            ])->toArray();
                    }
                ],
                'email_response' => [
                    'label' => __d('admin', 'Emails a donde se enviaran las respuestas'),
                    'help' => __d('admin', 'Indica los emails separados por comas y sin espacios (,)'),
                ],
                'submit_text' => [
                    'label' => __d('admin', 'Texto del botón de enviar formulario')
                ],
                'custom_email_from_bool' => 'Personalizar email remitente',
                'custom_email_from' => [
                    'label' =>  __d('admin', 'Email remitente'),
                    'help' => __d('admin', 'Al destinatario le aparecera este email como remitente'),
                    'show' => "content.custom_email_from_bool"
                ],
                'custom_privacy' => 'Texto personalizado para aceptación de privacidad',
                'legal_text' => 'Texto legal antes del botón de enviar',
                'privacy_section_id' => [
                    'label' => __d('admin', 'Pagina de política de privacidad'),
                    'help' => __d('admin', 'Página a donde se enlazará el texto de aceptar política de privacidad'),
                    'type' => 'select',
                    'options' => function ($crud) {
                        return TableRegistry::getTableLocator()->get('Section.Sections')->find('list')
                            ->where([
                                'Sections.action' => 'entry'
                            ])->toArray();
                    },
                    'show' => '!content.custom_privacy'
                ],
                'privacy_text' => [
                    'label' => 'Texto de aceptación de privacidad',
                    'show' => 'content.custom_privacy'
                ],
                'fields' => [
                    'label' => __d('admin', 'Campos'),
                    'type' => 'hasMany',
                    'template' => 'Form.fields.fields'
                ],
                'preview' => [
                    'type' => 'element',
                    'template' => 'Form.fields.preview'
                ],
                'cname' => [
                    'label' => __d('admin', 'Nombre clave para programación'),
                    'help' => __d('admin', 'Nombre clave para usar por el programador')
                ],
                'custom_mail_body' => __d('admin', 'Personalizar cuerpo de texto mail'),
                'mail_body' => [
                    'label' => __d('admin', 'Cuerpo de texto mail'),
                    'show' => 'content.custom_mail_body',
                ],
            ]);

        $elements = [
            'title',
            'has_subject',
            'subject',
            'custom_mail_body',
            'mail_body',
            'show_title',
            'body',
            'after_finish_option',
            'after_finish_text',
            'after_finish_section_id',
            'email_response',
            'custom_privacy',
            'privacy_section_id',
            'privacy_text',
            'custom_email_from_bool',
            'custom_email_from',
            'legal_text',
            'submit_text',
            'cname',
        ];

        $boxes = [
            [
                'title' => __d('admin', 'Datos del formulario'),
                'elements' => $elements,
            ],
        ];

        if (Plugin::isLoaded('Mailchimp') && Website::get('settings.mailchimp')) {
            $this->crud->addFields([
                'has_mailchimp_list' => __d('admin', 'Guardar usuarios en Mailchimp'),
                'mailchimp_list_id' => [
                    'label' => __d('admin', 'Lista de correo'),
                    'type' => 'select',
                    'options' => function ($crud) {
                        try {
                            $mc = new \DrewM\MailChimp\MailChimp(Website::get('settings.mailchimp'));
                            $lists = $mc->get('lists', [
                                'count' => 1000
                            ]);
                            $options = array('-- Ninguna --');
                            $_options = collection($lists['lists'])->combine('id', 'name')->toArray();
                            return array_merge($options, $_options);
                        } catch (\Throwable $th) {
                            return [];
                        }
                    },
                    'show' => 'content.has_mailchimp_list',
                    'change' => 'scope.send( scope.data.crudConfig.view.submit, scope.data.content, false)'

                ]
            ]);

            $boxes = array_merge($boxes, [
                [
                    'title' => __d('admin', 'Integración con Mailchimp'),
                    'elements' => [
                        'has_mailchimp_list',
                        'mailchimp_list_id'
                    ],
                ]
            ]);
        }

        $boxes = array_merge($boxes, [
            [
                'title' => __d('admin', 'Campos del formulario'),
                'class' => 'col-md-12',
                'elements' => [
                    'fields',
                ]
            ],
            [
                'title' => __d('admin', 'Previsualización'),
                'class' => 'col-md-12',
                'elements' => [
                    'preview',
                ]
            ]
        ]);

        $this->crud->addIndex('index', [
            'fields' => [
                'title',
                'email_response',
                'responses' => [
                    'label' => __d('admin', 'Respuestas'),
                    'type' => 'string',
                    'templateIndex' => 'Form.index.responses'
                ]
            ],
            'actionButtons' => ['create'],
            'saveButton' => false,
            'duplicate' => true
        ])
            ->setName([
                'singular' => __d('admin', 'Formulario'),
                'plural' => __d('admin', 'Formularios'),
            ])
            ->addView('create', [
                'columns' => [
                    [
                        'cols' => 8,
                        'box' => $boxes
                    ]
                ],
                'actionButtons' => ['create', 'index']
            ], ['update']);
    }

    public function beforeFind(Event $event, Query $query, $options)
    {
        $query->contain([
            'Fields.Options' => function ($q) {
                return $q->find('translations');
            }
        ]);
    }
}
