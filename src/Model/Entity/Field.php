<?php
namespace Form\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Behavior\Translate\TranslateTrait;use Cake\ORM\Entity;

/**
 * Field Entity.
 */
class Field extends Entity
{
  use CrudEntityTrait;
  use TranslateTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
    '*' => true,
    'id' => false,
    'options' => true,
    'required' => true,
  ];
}
