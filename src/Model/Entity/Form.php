<?php
namespace Form\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Behavior\Translate\TranslateTrait;use Cake\ORM\Entity;

/**
 * Form Entity.
 */
class Form extends Entity
{
  use CrudEntityTrait;
  use TranslateTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
    '*' => true,
    'id' => false,
    'fields' => true,
  ];
}
