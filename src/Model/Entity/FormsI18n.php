<?php
namespace Form\Model\Entity;

use Cake\ORM\Entity;

/**
 * FormsI18n Entity.
 */
class FormsI18n extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
