<?php

namespace Form\Model\Entity;

use ArrayObject;
use Cake\ORM\Entity;
use Cake\Event\Event;
use Cake\Event\EventManager;
use Manager\Model\Entity\CrudEntityTrait;

/**
 * Response Entity.
 *
 * @property int $id
 * @property int $form_id
 * @property \Form\Model\Entity\Form $form
 * @property string $data
 * @property string $ip
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Response extends Entity
{

    use CrudEntityTrait;


    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected $_virtual = [
        'html_admin_extra'
    ];

    public function toHtml()
    {
        $html = new ArrayObject();
        $event = new Event('Form.Model.Entity.Response.toHtml', $this, [$html]);
        EventManager::instance()->dispatch($event);

        foreach ($this->response_fields as $field) {
            $html[] = '<strong>' . $field->field->title . '</strong>: ' . $field->value_string;
        }

        // if (!empty($this->uri_referer)) {
        //     $html[] = '<strong>' . __d('app', 'URL desde donde se envió') . '</strong>: <a target="_blank" href="' . $this->uri_referer . '">' . $this->uri_referer . '</a>';
        // }

        return implode("\n<br>", (array)$html);
    }

    protected function _getHtmlAdminExtra()
    {
        $html = new ArrayObject();
        $event = new Event('Form.Model.Entity.Response.htmlAdminExtra', $this, [$html]);
        EventManager::instance()->dispatch($event);

        return implode("\n<br>", (array)$html);
    }

    protected function _getEmail()
    {
        foreach ($this->response_fields as $field) {
            if (!filter_var($field->value, FILTER_VALIDATE_EMAIL) === false) {
                return $field->value;
            }
        }
    }
}
