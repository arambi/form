<?php
namespace Form\Model\Entity;

use Cake\ORM\Entity;
use Cake\Routing\Router;


class ResponseField extends Entity
{
  protected $_accessible = [
      '*' => true,
      'id' => false,
  ];

  protected $_virtual = [
    'value_human'
  ];

  protected function _getValueHuman()
  {
    $value = $this->value;

    if( is_object( $this->field))
    {
      $method = '__get'. ucfirst( $this->field->type);

      if( method_exists( $this, $method))
      {
        return $this->$method( $value);
      }
    }

    return $value;
  }
  


  private function __getBoolean( $value)
  {
    if( $value)
    {
      return __d( 'app', 'Sí');
    }

    return __d( 'app', 'No');
  }

  private function __getFile( $value)
  {
    return json_decode( $value, true);
  }

  protected function _getValueString()
  {
    $value = $this->valueHuman;

    if( $this->field->type == 'file' && !empty( $value))
    {
      $value = implode( ', ', array_map( function( $value){
        return '<a target="_blank" download href="'. Router::url( $value ['paths'][0], true) .'">' . $value ['filename'] .'</a>';
      }, $value));
    }

    return $value;
  }
}
